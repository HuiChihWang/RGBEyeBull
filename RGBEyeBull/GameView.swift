//
//  ContentView.swift
//  RGBEyeBull
//
//  Created by Hui Chih Wang on 2021/5/5.
//

import SwiftUI

struct GameView: View {
    
    @State private var guessColor = RGB()
    @State private var ansColor = RGB.random()
    @State private var showAlert = false
    
    var body: some View {
        VStack {
            ColorView(color: ansColor, isShowAns: showAlert)
            
            ColorView(color: guessColor, isShowAns: showAlert)
            
            RGBSlider(guessColor: $guessColor)
                .padding(.horizontal, 20)
                .padding(.vertical, 10)
            
            Button("Hit Me") {
                showAlert = true
            }
            .buttonStyle(CapsuleButtonStyle(size: CGSize(width: 200, height: 50)))
            .foregroundColor(.blue)
            .alert(isPresented: $showAlert) {
                Alert(title: Text("Your Score"), message: Text("\(ansColor.score(with: guessColor))"), dismissButton: .default(Text("Next Round")) {
                    ansColor = RGB.random()
                })
            }
            
        }
    }
}

struct ColorSliderView: View {
    @Binding var colorValue: Double
    
    var body: some View {
        HStack {
            Text("0")
            Slider(value: $colorValue)
                .padding(.horizontal)
            Text("255")
        }
    }
}

struct RGBSlider: View {
    @Binding var guessColor: RGB
    
    var body: some View {
        VStack {
            ColorSliderView(colorValue: $guessColor.red)
                .accentColor(.red)
            ColorSliderView(colorValue: $guessColor.green)
                .accentColor(.green)
            ColorSliderView(colorValue: $guessColor.blue)
                .accentColor(.blue)
        }
    }
}


struct RGB {
    var red: Double = 0.5
    var green: Double = 0.5
    var blue: Double = 0.5
    
    var rgb: (red: Int, green: Int, blue: Int) {
        return(Int(255 * red), Int(255 * green), Int(255 * blue))
    }
    
    static func random() -> RGB {
        RGB(red: Double.random(in: 0...1), green: Double.random(in: 0...1), blue: Double.random(in: 0...1))
    }
    
    func score(with guess: RGB) -> Int {
        let redDiff = abs(guess.red - red)
        let greenDiff = abs(guess.green - green)
        let blueDiff = abs(guess.blue - blue)
        
        return 100 - Int((redDiff + blueDiff + greenDiff) / 3.0 * 100)
    }
}

extension Color {
    init(color: RGB) {
        self.init(red: color.red, green: color.green, blue: color.blue)
    }
    
    static let element = Color("Element")
    static let highlight = Color("Highlight")
    static let shadow = Color("Shadow")
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        GameView()
            .preferredColorScheme(.dark)
    }
}


struct CapsuleButtonStyle: ButtonStyle {
    let size: CGSize
    
    func makeBody(configuration: Configuration) -> some View {
        let background = Group {
            if !configuration.isPressed {
                Capsule()
                    .flyshadow()
            }
            else {
                Capsule()
            }
        }
        .foregroundColor(.element)
        
        return configuration.label
            .frame(width: size.width, height: size.height, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .background(background)
    }
}

extension View {
    func flyshadow() -> some View {
        self
            .shadow(color: .shadow, radius: 5)
    }
}





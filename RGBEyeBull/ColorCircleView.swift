//
//  ColorCircleView.swift
//  RGBEyeBull
//
//  Created by Hui Chih Wang on 2021/5/6.
//

import SwiftUI

struct ColorView: View {
    private var color: RGB
    private var isShowAns: Bool
    
    init(color: RGB, isShowAns: Bool) {
        self.color = color
        self.isShowAns = isShowAns
    }
    var body: some View {
            VStack {
                ColorCircleView(color: color)
                    .frame(width: 250, height: 250, alignment: .center)
                ColorLabelView(color: color, isShowAns: isShowAns)
                    .padding(.top, 10)
            }
    }
}

struct ColorViewPreview: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.gray
            ColorView(color: RGB.random(), isShowAns: true)
        }
        .frame(width: 500, height: 500, alignment: .center)
        .previewLayout(.sizeThatFits)
    }
}



struct ColorCircleView: View {
    private var color: RGB
    
    init(color: RGB) {
        self.color = color
    }
    
    var body: some View {
        ZStack {
            Circle()
                .foregroundColor(.element)
                .flyshadow()
            
            Circle()
                .foregroundColor(Color(color: color))
                .padding(20)
        }
    }
}

struct ColorLabelView: View {
    private var color: RGB
    private var isShowAns: Bool
    
    init(color: RGB, isShowAns: Bool) {
        self.color = color
        self.isShowAns = isShowAns
    }
    
    var body: some View {
        HStack(spacing: 15){
            Text("Red: \(isShowAns ? String(color.rgb.red) : "??")")
            Text("Green: \(isShowAns ? String(color.rgb.green) : "??")")
            Text("Blue: \(isShowAns ? String(color.rgb.blue) : "??")")
        }
    }
}

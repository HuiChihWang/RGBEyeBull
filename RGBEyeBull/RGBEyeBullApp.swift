//
//  RGBEyeBullApp.swift
//  RGBEyeBull
//
//  Created by Hui Chih Wang on 2021/5/5.
//

import SwiftUI

@main
struct RGBEyeBullApp: App {
    var body: some Scene {
        WindowGroup {
            WelcomView()
        }
    }
}

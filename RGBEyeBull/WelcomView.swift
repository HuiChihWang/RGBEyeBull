//
//  WelcomView.swift
//  RGBEyeBull
//
//  Created by Hui Chih Wang on 2021/5/6.
//

import SwiftUI

struct WelcomView: View {
    let screenSize = UIScreen.main.bounds
    
    var body: some View {
        VStack {
            TitleView(title: "RGB  Bulls  Eye", subTitle: "Make a good decision color")
                .padding(.bottom, 50)
            
            
            RegisterView()
        }
        .background(
            BackgroundImageView()
                .frame(height: screenSize.height * 0.7)
    )
    }
}

struct WelcomView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomView()
    }
}

struct BackgroundImageView: View {
    var body: some View {
        Image("Welcome")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .opacity(0.5)
            .blur(radius: 3)
    }
}

extension View {
    func foreground(with gradient: Gradient) -> some View {
        self
            .overlay(LinearGradient(gradient: gradient, startPoint: .topLeading, endPoint: .bottomTrailing))
            .mask(self)
    }
    
    func rectangleBackground(color: Color) -> some View {
        self
            .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
            .background(
                Color.white.opacity(0.8)
                    .cornerRadius(10)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(lineWidth: 2)
                    )
            )
            .padding(10)
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(lineWidth: 2)
            )
        
    }
}

struct TitleView: View {
    let title: String
    let subTitle: String
    var body: some View {
        VStack {
            Text(title)
                .font(.system(size: 40, weight: .semibold, design: .rounded))
                .foreground(with: Gradient(colors: [Color.red, Color.blue]))
            
            Text(subTitle)
                .font(.system(size: 17, weight: .regular, design: .monospaced))
        }
        .rectangleBackground(color: .blue)
    }
}

struct ResponsiveRoundedTextFieldStyle: TextFieldStyle {
    var isEditing: Bool
    
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(.horizontal, 5)
            .padding(.vertical, 10)
            .background(
            RoundedRectangle(cornerRadius: 10)
                .foregroundColor(.white)
                .opacity(0.9)
                .overlay(
                    Group {
                        if isEditing {
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color.blue, lineWidth: 3)
                        }
                    }
                )
            )
    }
}

struct RegisterView: View {
    @State private var userName: String = ""
    @State private var isEditUserName = false
    
    var body: some View {
        VStack {
            TextField("Enter Your Name", text: $userName) { isEditing in
                self.isEditUserName = true
            }
            .textFieldStyle(ResponsiveRoundedTextFieldStyle(isEditing: isEditUserName))
            .padding(.horizontal, 25)
            
            Button(action: {
                
            }, label: {
                HStack {
                    Image(systemName: "gamecontroller")
                    Text("Play the Game")
                }
                .font(.system(.title2))
                .padding(10)
                .background(
                    RoundedRectangle(cornerRadius: 25)
                        .foregroundColor(.white)
                        .shadow(radius: 10)
                )
                
            })
            .padding(.top, 20)
        }
    }
}
